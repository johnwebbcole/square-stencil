// title      : squareStencil
// author     : John Cole
// license    : ISC License
// file       : squareStencil.jscad

/* exported main, getParameterDefinitions */

function getParameterDefinitions() {
  return [
    {
      name: 'width',
      type: 'int',
      initial: 4,
      caption: 'Width (inch):'
    },
    {
      name: 'inset',
      type: 'int',
      initial: 2,
      caption: 'Inset (inch):'
    },
    {
      name: 'gap',
      type: 'int',
      initial: 1,
      caption: 'Gap (mm):'
    }
    // {
    //   name: 'resolution',
    //   type: 'choice',
    //   values: [0, 1, 2, 3, 4],
    //   captions: [
    //     'very low (6,16)',
    //     'low (8,24)',
    //     'normal (12,32)',
    //     'high (24,64)',
    //     'very high (48,128)'
    //   ],
    //   initial: 2,
    //   caption: 'Resolution:'
    // }
  ];
}

function main(params) {
  // var resolutions = [[6, 16], [8, 24], [12, 32], [24, 64], [48, 128]];
  // CSG.defaultResolution3D = resolutions[params.resolution][0];
  // CSG.defaultResolution2D = resolutions[params.resolution][1];
  util.init(CSG);

  var base = Parts.Cube([util.inch(params.width), util.inch(params.width), 2])
    .chamfer(1, 'z+')
    .color('orange')
    .Center();

  var gapWidth = params.gap / 2;
  var cut = Parts.Cube([
    util.inch(params.inset + util.cm(gapWidth / 10)),
    util.inch(params.inset + util.cm(gapWidth / 10)),
    4
  ]).Center();

  var inset = Parts.Cube([
    util.inch(params.inset) - gapWidth,
    util.inch(params.inset) - gapWidth,
    2
  ])
    .Center()
    .color('blue');

  var s = inset.size();
  var label = util
    .label(
      `${params.inset} inch${params.inset > 1 ? 's' : ''}`,
      0,
      0,
      2,
      util.nearest.over(0.5, 0.15)
    )
    .fit([s.x - 10, s.y - 10, 0], true)
    .snap(inset, 'z', 'outside-')
    .align(inset, 'xy');

  var gusset = Parts.Cube([util.inch(params.inset) + gapWidth * 2, 10, 1])
    .Center()
    .color('red');

  return [base.subtract(cut), inset, gusset, gusset.rotateZ(90), label];
}

// ********************************************************
// Other jscad libraries are injected here.  Do not remove.
// Install jscad libraries using NPM
// ********************************************************
// include:js
// endinject
